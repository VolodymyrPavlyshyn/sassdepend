/**
 * Created by vpavlyshyn on 13.10.14.
 */
/**
 * Created by vpavlyshyn on 13.10.14.
 */
var exec = require('child_process').exec,
    fs = require('fs'),
    _ = require('lodash'),
    child,
    optimist = require('optimist')
        .usage('generate dot file for sass imports: $0 -o result.dot -l f -i sass_import.svg /sass/folder/root')
        .alias('b', 'base')
        .boolean('d','save debugging files')
        .describe('b', 'base folder')
        .describe('o', 'dot file name . xdot viewer could be used for dot file (sudo apt-get install xdot)')
        .default('o', 'result.dot')
        .describe('i','image format. ! graphviz tools required (sudo apt-get install graphviz)')
        .describe('l', 'label : p - full path, f - file-name, r - relative, u - uid')
        .default('l','r')
        .describe('h', 'Display the usage')
        .alias('h', 'help'),
        argv = optimist.argv,
        grepProcessor = require('../lib/grepProcessor').grepProcessor({base: argv.b || _.last(argv._)});
if (argv.help) {
    optimist.showHelp();
}
child = exec(grepProcessor.getCommand(), function (error, stdout) {
    //console.log(stdout);
    var pairProcessor = require('../lib/pairProcessor').pairProcessor({base: argv.b || _.last(argv._), label: argv.l}),
        processed = pairProcessor.process(grepProcessor.dependentPairs(stdout));
    if(argv.d) {
        fs.writeFileSync('result_1_grep.txt', stdout);
        console.log(grepProcessor.asJson());
        if(grepProcessor.hasErrors()){
            console.log('--------------- parse errors -+-----------------------------');
            console.log(grepProcessor.getErrors());
            fs.writeFileSync('result_2_errors.json', grepProcessor.getErrors());
        }
        fs.writeFileSync('result_1.json', grepProcessor.asJson());
        console.log('---------------- processed pairs --------------------------------');
        fs.writeFileSync('result_2.json', pairProcessor.asJson());
        console.log(pairProcessor.asJson());
    }
    fs.writeFileSync(argv.o || 'out.dot', pairProcessor.asDot());

    if(argv.i){
        pairProcessor.picture(argv.i);
    }

});
