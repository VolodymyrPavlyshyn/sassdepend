/**
 * Created by vpavlyshyn on 13.10.14.
 */
module.exports.pairProcessor = function graphProcessor(moptions) {
    var defaults = {
            extension: 'scss',
            base: './'
        },
        _ = require('lodash'),
        fs = require('fs'),
        path = require('path'),
        result_init = {
            all: [],
            reference: [],
            roots: [],
            nodesId: {},
            pathToId: {},
            dependency: {}
        },
        result = _.clone(result_init),
        graph_result ,
        loptions = _.extend(defaults, moptions),
        impl = {
            processPairs: function pairProcessor_processPairs(pairs) {
                result = _.clone(result_init);
                _.each(pairs, function (pair) {
                    impl.registerPair(pair);
                });
                result.roots = _.difference(result.all, result.reference);
                return result;
            },
            registerPath: function registerPath(path) {
                if (!path) return;
                var uid = result.pathToId[path];
                if (!uid) {
                    uid = _.uniqueId('ss');
                    result.pathToId[path] = uid;
                    result.nodesId[uid] = {uid: uid, path: path};
                    result.all.push(uid);
                }
                return uid;
            },
            registerPair: function (pair) {
                var dependency, fileId,
                    dependencyId;
                if (pair) {
                    fileId = impl.registerPath(pair.file);
                    dependencyId = impl.registerPath(pair.on);
                    dependency = result.dependency[fileId];
                    if (!dependency) {
                        result.dependency[fileId] = [];
                        dependency = result.dependency[fileId];
                    }
                    if (!_.contains(dependency, pair.file)) {
                        dependency.push(dependencyId);
                        result.reference.push(dependencyId);
                    }
                }

            },
            // graphviz stuff better to extract
            asDot: function (input) {
                var _in = input || result, __g;
                var util = require('util'),

                    ROOT_ID = 'root',
                    dotUtils = {
                        _dot: {
                            nodes: [],
                            edges: []
                        },
                        addNode: function (id, options) {
                            dotUtils._dot.nodes.push(id + ((options && options.label ) ? ' [label=\"' + options.label + '\"]' : '') + ';\n');
                        },
                        addEdge: function (id, connection) {
                            dotUtils._dot.edges.push(id + ' -> ' + connection + ';\n');
                        },
                        to_dot: function () {
                            return ' digraph Sass { \n ' + dotUtils._dot.nodes.join('') + '\n\n' + dotUtils._dot.edges.join('') + '\n\n' + ' } \n\n';
                        }
                    },
                    _gvUtils = {
                        initGraph: function () {
                            var  graphviz = require('graphviz'),
                                _graph = graphviz.digraph('Sass');
                            _graph.addNode(ROOT_ID);
                            return _graph;
                        },
                        createGraph: function (g, data) {
                            var _data = data || result;
                            var _g = g || {};
                            _g = _gvUtils.initGraph();
                            _.each(_data.nodesId, function (node) {
                                _gvUtils.addNormalNode(_g, node);
                            });
                            _.each(_data.dependency, function (dependon, key) {
                                _gvUtils.addConnections(_g, key, dependon);
                            });
                            _gvUtils.addRoots(_g, _data.roots);
                            graph_result = _g;
                            return _g.to_dot();

                        },
                        addNormalNode: function (gr, item, options) {
                            var _options = _.extend(loptions, options),
                                tooltip = path.relative(_options.base, item.path),
                                label =  path.basename(item.path,'.scss');
                                switch (_options.label){
                                    case 'p':
                                        label = item.path;
                                    break;
                                    case 'u':
                                        label = item.uid;
                                        break;
                                    case 'r':
                                        label = tooltip;
                                        break;
                                    case 'f':
                                        label = path.basename(item.path,'.scss');
                                        break;
                                    default :
                                        label = tooltip;
                                        break;
                                }
                            gr.addNode(item.uid, _.extend({label: label, tooltip: tooltip }, options));
                        },
                        addConnections: function (gr, id, connections) {
                            _.forEach(connections, function (connection) {
                                gr.addEdge(id, connection);
                            });
                        },
                        addRoots: function (gr, roots) {
                            _gvUtils.addConnections(gr, ROOT_ID, roots);
                        }
                    };

                return _gvUtils.createGraph(__g, result);
            },
            asImage: function (pathName) {
                graph_result.output(path.extname(pathName).slice(1),pathName);
            }
        },
        IPublic = {
            process: impl.processPairs,
            asJson: function () {
                return JSON.stringify(result);
            },
            asDot: impl.asDot,
            picture: impl.asImage

        };
    return IPublic;
};
