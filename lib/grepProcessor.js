module.exports.grepProcessor = function grepProcessor(moptions) {
    var defaults = {
            splitter: ':@import \"',
            pattern: '@import',
            tail: '\";',
            extension: 'scss',
            partialMarcker: '_',
            base: './',
            filter : ['compass/css3','compass','css3','compass/utilities/sprites'],
            processMissed: true
        },
        _ = require('lodash'),
        fs = require('fs'),
        path = require('path'),
        result = [],
        errors = {
          lines_all:[],
          parse:[],
          filtered:[],
          notExist:[]
        },
        loptions = _.extend(defaults, moptions),
        linesCount = 0,
    impl = {
        getCommand: function grepProcessor_getCommand() {
            return 'grep -R -e \'' + loptions.pattern + '\' ' + loptions.base;
        },
        getDependentPaths: function grepProcessor_getDependentPaths(list) {
            var raw , pair , dependon, lines = list.split(loptions.tail),linesCount = lines.length;
            if(linesCount === 0){
                console.log('No dependencies found');
            }
            _.each(lines, function (line) {
                var pair = impl._normalizePath(impl._extractPair(line.replace(/[\r\n]/g,'')));
                if (pair) {
                    result.push(pair);
                }
            });

            return result.slice(0);
        },
        _extractPair: function grepProcessor_extractPair(line) {
            var raw,basename,basedir,dependon, missed = false;
            if (!line && line.length < 2) {
                console.warn('WARN : not parsed ' + line + 'line to short');
                errors.parse.push(line);
                errors.lines_all.push(line + ':00');
                return null;
            }
            raw = line.split(loptions.splitter);
            if (raw.length !== 2) {
                console.warn('WARN : not parsed ' + line + 'could not split to pair split result' + JSON.stringify(raw));
                errors.parse.push(line);
                errors.lines_all.push(line + ':07');
                return null
            }
            dependon = raw[1];
            if (_.contains(loptions.filter,dependon)){
                errors.lines_all.push(line + ':06');
                console.log(line + ' filtered line ');
                errors.filtered.push(dependon);
                return null;
            }
            if (dependon.indexOf('.' + loptions.extension) == -1) {
                dependon = dependon + '.' + loptions.extension;
            }
            return {
                file: raw[0],
                on: dependon
            }
        },
        _normalizePath: function grepProcessor_extractPair(pair) {
            if (!pair) {
                return  null;
            }
            var missed, basename,
                baseDir = path.normalize(path.dirname(pair.file)),
                dependon = path.normalize(path.resolve(baseDir, pair.on)),
                depends_original = dependon.slice(0);

            if (!fs.existsSync(pair.file)){
                console.error(pair.file + 'file not exist');
                errors.lines_all.push(JSON.stringify(pair) + ':03 file ' + pair.file + ' :05');
                errors.notExist.push(pair.file);
                missed = true;
            }

            if (!fs.existsSync(dependon)){
                basename =  path.basename(dependon);
                 if (basename[0] === loptions.partialMarcker) {
                    console.error(dependon + 'file not exist');
                    errors.lines_all.push(line + ':03 file ' + dependon + ' :04');
                    errors.notExist.push(dependon);
                    missed = true;
                 } else {
                     console.log(dependon + 'is partial');
                     dependon = path.dirname(dependon)+path.sep + loptions.partialMarcker + basename;
                     console.log('partial resolved to '+ dependon);
                 }
            }

            if (!fs.existsSync(dependon)) {
                console.log('try to resolve on root directory');
                dependon = loptions.base + path.sep + path.basename(dependon);
            }
            if (!fs.existsSync(dependon)) {
                console.log('try to resolve on root directory for not partial');
                dependon = loptions.base + path.sep + path.basename(depends_original);
            }
            if (!fs.existsSync(dependon)) {
                console.error(dependon + 'file not exist');
                errors.lines_all.push(JSON.stringify(pair) + ':03 file ' + depends_original + ' => ' + dependon + ' :05');
                errors.notExist.push(dependon);
                missed = true;
            }

            if (missed && !loptions.processMissed){
                return null;
            }

            return {
                file: path.normalize(pair.file),
                on: path.normalize(dependon)

            }

        }
    },
        IPublic = {
            getCommand: impl.getCommand,
            dependentPairs: impl.getDependentPaths,
            getErrors : function(){
                return JSON.stringify(errors);
            },
            hasErrors : function(){
                return errors && errors.lines_all && errors.lines_all.length > 0;
            },
            asJson: function () {
                return JSON.stringify(result);
            }
        };
    return IPublic;
};
